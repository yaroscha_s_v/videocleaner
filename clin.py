import argparse
import time
import json
import pickle
import sys
import os
from VideoCliner import  Img2VecUtil,VideoCliner


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-video',nargs='?', help='Path to video')
    parser.add_argument('-no_ads',action='store_true', help='Setting the flag disables ads search')
    parser.add_argument('-overlay',action='store_true', help='Setting the flag includes searching for overlays')

    cli = parser.parse_args()
    FPS = 23
    is_saved = not True#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if cli.video:
        util = Img2VecUtil()
        cliner = VideoCliner()
        video_path = os.path.split(cli.video)
        advertising_info = {'frames_intervals':[],'ms_intervals':[],'overlays_intervals':[],'ms_overlays_intervals':[]}
        print(f'Start clining for: {cli.video}')
        s = time.time()

        if not cli.no_ads: # Перевірка на вміст реклами     
            #path = 'DB_Json'
            path = os.path.join(sys.path[0],'DB_Json')
            # Конвертація відео в вектор
            if not os.path.exists(f'{cli.video}.json'):
                video_vec = util.video2vec(video_path[0],video_path[1],FPS)
                if is_saved:
                    with open(f'{cli.video}.json','wb') as f:
                        pickle.dump(video_vec,f)
            else:
                with open(f'{cli.video}.json','rb') as f:
                    video_vec = pickle.load(f)

            print('converted video')
            advertising_intervals = []
            for folder in os.listdir(path):
                advertising_vec = []
                for file in os.listdir(os.path.join(path,folder)):          
                    with open(os.path.join(path,folder,file),'rb') as f:
                        advertising_vec.append(pickle.load(f))

                intervals = cliner.find_advertising(video_vec,advertising_vec,util,fps=FPS)
                if intervals:
                    advertising_intervals.append(intervals)
                    print(f'{folder}: {intervals}')
                else:
                    print(f'{folder}: [Empty]')
            
        
            advertising_info['frames_intervals'] = advertising_intervals #str(advertising_intervals) 
            for intervals in advertising_intervals:
                rez = util.get_ms_intervals(cli.video,intervals)
                advertising_info['ms_intervals'].append(rez)
        else:
            print('Режим пошуку реклами відключено!')


        if cli.overlay: #Пошук оверлеїв

            if not os.path.exists(f'{cli.video}_over.json'):
                overlay_infos = util.get_overlay_info(video_path[0],video_path[1],FPS)
                if is_saved:
                    with open(f'{cli.video}_over.json','wb') as f:
                        pickle.dump(overlay_infos,f)
            else:
                with open(f'{cli.video}_over.json','rb') as f:
                    overlay_infos = pickle.load(f)

            overlay_intervals = cliner.find_overlay(overlay_infos,FPS)
            #print(dict(overlay_intervals))
            
            advertising_info['overlays_intervals'] = dict(overlay_intervals)

            for intervals in overlay_intervals.values():
                rez = util.get_ms_intervals(cli.video,intervals)
                advertising_info['ms_overlays_intervals'].append(rez) 
           
        else:
            print('Режим пошуку оверлеїв відключено!')

        print(advertising_info)

        filename = os.path.splitext(video_path[1])[0]
        with open(f'{filename}.json','w') as f:
            json.dump(advertising_info,f)
            
            
        print(time.time()-s)
    else:
        print('Set option -video')