import cv2
import os
import json
import pickle
import collections
from sklearn.metrics.pairwise import cosine_similarity
from tensorflow.keras.models import Model,load_model
from tensorflow.keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D,concatenate,Dropout
from tensorflow.keras.applications.imagenet_utils import preprocess_input
import numpy as np

class Img2VecUtil:
    
    def __init__(self,al=3/4,shape=(160,160),model_path='Model/detector160x160.h5'):
        """
        al - доля оригінального зображення що відбирається для перетворення
        shape - форма зображення з яким працює модель
        model_path - шлях до моделі
        """       
        self.shape = shape
        self.al = al
        input_img = Input(shape=( shape[0], shape[1],3))
        x = Conv2D(128, (3, 3), activation='relu', padding='same')(input_img)
        x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D((2, 2), padding='same')(x)
        x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
        x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D((2, 2), padding='same')(x)
        x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
        x = Conv2D(32, (3, 3), activation='relu', padding='same')(x)
        x = MaxPooling2D((2, 2), padding='same')(x)
        x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
        x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
        encoded = MaxPooling2D((2, 2), padding='same',name='encoder')(x)
        x =  Conv2D(16, (3, 3), activation='relu', padding='same')(encoded)
        x =  Conv2D(16, (3, 3), activation='relu', padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        x =  Conv2D(32, (3, 3), activation='relu', padding='same')(x)
        x =  Conv2D(32, (3, 3), activation='relu', padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        x =  Conv2D(64, (3, 3), activation='relu', padding='same')(x)
        x =  Conv2D(64, (3, 3), activation='relu', padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        x =  Conv2D(128, (3, 3), activation='relu', padding='same')(x)
        x =  Conv2D(128, (3, 3), activation='relu', padding='same')(x)
        x = UpSampling2D((2, 2))(x)
        decoded =  Conv2D(3, (3, 3), activation='sigmoid', padding='same')(x)
        autoencoder = Model(input_img, decoded)
        autoencoder.load_weights(model_path) 
        self.model = Model(inputs=autoencoder.input,outputs=autoencoder.get_layer(name='encoder').output)
        self.black = None
        self.white = None
        
    def get_black(self):
        """
        Повертає векторне представлення абсолютно чорного кадру
        """
        if self.black is None:
            #Створює чорний кадр і знаходить його векторне представлення
            im = np.array([0]*self.shape[0]*self.shape[1]*3)     
            im = np.reshape(im,(1,self.shape[0],self.shape[1],3))
            self.black = self.model.predict(im)
            self.black = np.reshape(self.black,(self.black.shape[0],self.black.shape[1]*self.black.shape[2]*self.black.shape[3]))
      
        return self.black

    def get_white(self):
        """
        Повертає векторне представлення абсолютно білого кадру
        """
        if self.white is None:
            #Створює білий кадр і знаходить його векторне представлення
            im = np.array([255]*self.shape[0]*self.shape[1]*3)     
            im = np.reshape(im,(1,self.shape[0],self.shape[1],3))
            self.white = self.model.predict(im)
            self.white = np.reshape(self.white,(self.white.shape[0],self.white.shape[1]*self.white.shape[2]*self.white.shape[3]))
      
        return self.white
   
    def predict(self,frame_list):
        """
        Перетворює список зображень у їх векторне представлення
        Повертає список векторних представлень
        """
        vectors =  self.model.predict(np.array(frame_list))
        vectors = np.reshape(vectors,(vectors.shape[0],vectors.shape[1]*vectors.shape[2]*vectors.shape[3]))      
        return vectors.tolist()
        
    
    def video2vec(self,path, video_name, step=23):
        """
        Перетворює відеофайл, що розміщені в path, за допомогою  model в список векторних представлень кадрів
        path - шлях до відео
        video_name - імя існуючого відео
        step - крок через який зчитуються кадри відео
        """      
        cap = cv2.VideoCapture(os.path.join(path, video_name))
        _ = cap.set(cv2.CAP_PROP_POS_AVI_RATIO,0)
        
        frame_list = []
        count_frame = 0
        while(True):
            
            #Пропуск кадрів
            while count_frame%step != 0:
                ret = cap.grab()
                count_frame += 1
                    
            ret, frame = cap.read()       
            
            if not ret:break
   
            y = frame.shape[0]/2
            x = frame.shape[1]/2
            frame = frame[int(y-self.al*y):int(y+self.al*y),int(x-self.al*x):int(x+self.al*x)]            
            frame = cv2.resize(frame, self.shape)
            frame = frame/ 255.
                     
            frame_list.append(frame)
            count_frame += 1

        cap.release()

        return self.predict(frame_list)
    
    
    def images2vec(self,path,crop=True):
        """
        Перетворює зображення кадрів реклами, що розміщені в path, за допомогою  model 
        в список їх  векторних представлень
        path - шлях до папки з кадрами
        Повертає список векторних представлень
        """      
        frame_list = []
        for file in os.listdir(path):            
            frame = cv2.imread(os.path.join(path,file))
            
            t = frame.shape[1]/ frame.shape[0]              
            y = frame.shape[0]/2
            x = frame.shape[1]/2

            if crop : 
                if t > 2 :
                    frame1 = frame[int(y-self.al*y):int(y+self.al*y), int(x-self.al*x):int(x+self.al*x)]           
                    frame1 = cv2.resize(frame1, self.shape)
                    frame1 = frame1/ 255.           
                    frame_list.append(frame1)
                    #Розширене для випадку полос з верху
                    for scale in [1.8]:
                        y_s = int(y-self.al*x/scale) if int(y-self.al*x/scale) > 0 else 0
                        y_e = int(y+self.al*x/scale) if int(y+self.al*x/scale) < frame.shape[0] else frame.shape[0]

                        frame2 = frame[y_s:y_e, int(x-self.al*x):int(x+self.al*x)]                   
                        frame2 = cv2.resize(frame2, self.shape)
                        frame2 = frame2/ 255.           
                        frame_list.append(frame2)
                else:
                #Розширене для випадку полос з боку
                    for scale in [2.36,1.83]:

                        x_s = int(x-self.al*y*scale) if int(x-self.al*y*scale) > 0 else 0
                        x_e = int(x+self.al*y*scale) if int(x+self.al*y*scale) < frame.shape[1] else frame.shape[1]

                        frame3 = frame[int(y-self.al*y):int(y+self.al*y), x_s :x_e]            
                        frame3 = cv2.resize(frame3, self.shape)
                        frame3 = frame3/ 255.           
                        frame_list.append(frame3)
            else:
                frame4 = cv2.resize(frame, self.shape)
                frame4 = frame4/ 255.           
                frame_list.append(frame4)

        return self.predict(frame_list)
       
    
    def convert_reklama_to_json(self,path,folder_json = 'DB_Json',rewrite=False):
        """
            Конвертує кадри реклами з папок, що розташовані по шляху path 
            у відповідні векторні представлення та зберігає їх
            у вигляді json з іменем папки реклами
            path - шлях до папок з кадрами реклам
            folder_json -  папка де будуть зберігатися векторні представлення
            rewrite - чи перезаписувати існуючі векторні представлення
        """
        
        if not os.path.exists(folder_json):
            os.mkdir(folder_json)
            
        for folder in os.listdir(path):
            json_name = os.path.join(folder_json,f'{folder}.json')
            print(json_name)
            if os.path.exists(json_name) and not rewrite:
                continue
                
            resoult = self.images2vec(os.path.join(path,folder))

            with open(json_name,'wb') as f:
                pickle.dump(resoult,f)#!!!!!!!!!!!!!!!!!!


    def _create_frames(self,file,folder):
        """
            Створення розкадровки з відео файлу file,
            в новостворену папку з іменем файлу яка зберігається в папці folder
            file - повне імя файлу  
        """

        count_frame = 0
        cap = cv2.VideoCapture(file)
        _ = cap.set(cv2.CAP_PROP_POS_AVI_RATIO,0)

        file =os.path.basename(file)
        filename = os.path.splitext(file)[0]
        sub_folder = os.path.join(folder,filename)
            
        if not os.path.exists(sub_folder):
            os.mkdir(sub_folder)

        while(True):
            ret, frame = cap.read()
            if not ret : break
            cv2.imwrite(os.path.join(sub_folder,f"{filename}_{count_frame}.jpg"),frame)
            count_frame += 1
        cap.release()

    
    def create_frames_from_path(self,folder,dataset_folder):
        """Проход  по всіх папках з рекламою та розкадровка відповідних відео
           folder - Папка з паппками реклам
           dataset_folder - папка де зберігаються папки з кадрами реклам
        """
        if not os.path.exists(dataset_folder):
            os.mkdir(dataset_folder)
        #Subfolders
        files = [os.path.join(folder,fol,file) for fol in os.listdir(folder) for file in os.listdir(os.path.join(folder,fol)) ]

        for file in files:
            self._create_frames(file,dataset_folder)
            
            
    def get_ms_intervals(self,video_file,interval_list):
        """
        Конвертує інтервали номерів кадрів у інтервали мілісекунд
        video_file - повне імя відеофайлу
        interval_list - список інтервалів номерів кадру
        """
        cap = cv2.VideoCapture(video_file)
        ms_list = []
        for interval_list in interval_list:
            _ = cap.set(cv2.CAP_PROP_POS_FRAMES,interval_list[0])
            start_time = cap.get(cv2.CAP_PROP_POS_MSEC)
            _ = cap.set(cv2.CAP_PROP_POS_FRAMES,interval_list[1])
            end_time = cap.get(cv2.CAP_PROP_POS_MSEC)
            ms_list.append((start_time,end_time))
        cap.release()
        return ms_list


    def get_overlay_info(self,path, video_name, step=23,model_detection_path='Model/find_overlay160x160_4.h5'):
        """
        Перетворює відеофайл, що розміщені в path, за допомогою  model в список векторних представлень кадрів
        path - шлях до відео
        video_name - імя існуючого відео
        step - крок через який зчитуються кадри відео
        model_detection_path - шлях до моделі детектора оверлеїв
        """    
        model = load_model(model_detection_path)  
        cap = cv2.VideoCapture(os.path.join(path, video_name))
        _ = cap.set(cv2.CAP_PROP_POS_AVI_RATIO,0)
        
        frame_list = []
        count_frame = 0
        while(True):
            
            #Пропуск кадрів
            while count_frame%step != 0:
                ret = cap.grab()
                count_frame += 1
            ret, frame = cap.read()       
            if not ret:break
            frame = cv2.resize(frame, self.shape)   
            frame_list.append(frame)
            count_frame += 1

        cap.release()

        data = preprocess_input(np.array(frame_list))
        frame_list.clear()

        return model.predict(data)



class VideoCliner:
        
    def _breaks_list(self,infos,fps,dist=20):
        """
        Розбиває список з номерами кадрів реклами на підсписки, в залежності скільки реклам локалізовано
        критерієм розбиття є перевищення відстані dist між сусідніми елементами
        infos -  вхідний список
        fps -  частота з якою кадри відбиралися з відеофайлу
        dist - відстань в секундах, яка можлива між двома рекламами
        Параметри підбераються експерементально
        Повертає список списків кадрів реклам 
        """
        resoult = []
        start = 0
        for i in range(len(infos)-1):
            if infos[i+1]-infos[i] > dist:
                resoult.append(infos[start:i+1])
                start = i+1
        resoult.append(infos[start:]) 
        return resoult
    
    def _get_other_frames(self,similarity_matrix,start_index,step,blacks=None):
        """
        Уточнює позиці реклами відносно  start_index
        similarity_matrix - Матриця косинусів розрахована для відео та конкретного вектору реклами
        start_index - індекс кадру в якому точно реклама
        step - крок по кадрах:
                            1 - для зростання номерів
                            -1 - для спадання номерів
        Повертає початковий або кінцевий кадр реклами, в залежності від значення step
        """
        # index - поточний номер кадру відео
        index = start_index + step #Старт із знайденого фрейму зміщеного на step
        count_empty_step = 0 #Кількість кадрів підряд які не ідентифікувалися як реклама, для них count == 0
        acuraci_ident = 0.96
        empty_step = 4
        
        while True:
            count = 0
            if index < 1:
                return 0
            if index >= similarity_matrix.shape[0]:
                return similarity_matrix.shape[0] - 1

            for i,z in enumerate(similarity_matrix[index]):
                if z > acuraci_ident:
                    count += 1
            # Можливо добавити поріг в 9
            if count == 0:#
                count_empty_step += 1
                if count_empty_step > empty_step:#Поріг в 4 кадри які підряд можуть не ідентифікуватися
                    return index-step*count_empty_step
            else:
                count_empty_step = 0 

            index = index + step

    def _reduce_list(self,list_intervals):
    #Видаляє зі списка підінтервали які входять в інші інтервали списка 
        delete = True
        start_index = 0
        while delete:
            delete = False
            if start_index > len(list_intervals)-1:
                break
            elem = list_intervals[start_index]

            for item in list_intervals[start_index+1:]:
                if item[0]<=elem[0] and elem[1]<=item[1]:
                    list_intervals.remove(elem) # del elem
                    start_index -= 1
                    delete = True
                    break
                if elem[0]<=item[0] and item[1]<=elem[1]:
                    list_intervals.remove(item) # del item
                    delete = True
            start_index += 1
        return list_intervals
            
            
    def find_advertising(self,video,advertisings_list,img_vec_util,fps):
     
        acuraci_black =  0.90 #Точність для пошуку чорних кадрів
        acuraci_ident =  0.995 #Точність для пошуку кадрів реклами

        porog_find_count = 1 #9поріг подібності сусідніх кадрів для знайдених співпадінь

        X = np.stack(video)
        advertising_intervals = [] # Список інтервалів знайдених реклам

        #Проходимо по всіх рекламах
        for advertising in advertisings_list:
            Y = np.stack(advertising)
            similarity_matrix = cosine_similarity(X, Y)

            frame_list = [] #Знайдені номери кадрів відео з рекламою
            #Підрахунок кількості подібних кадрів та відсіяти ті які не подолали поріг porog_find_count
            for index in range(similarity_matrix.shape[0]):
                count_equ_frame = 0 
                for z in similarity_matrix[index]:
                    if z > acuraci_ident:
                        count_equ_frame += 1

                if count_equ_frame > porog_find_count:
                    frame_list.append(index)#(index*fps)       

            # print('Frame list',frame_list)
            for reklama_list in self._breaks_list(frame_list,fps,40):
                if reklama_list:
                    # print(reklama_list)
                    if len(reklama_list)<1: #3Відкидаємо списки в яких кількість кадрів не перевищує 3
                        print('<2')
                        continue
                    #Розширюємо інтервали    
                    min_index_frame = np.min(reklama_list)#//fps
                    max_index_frame = np.max(reklama_list)#//fps
                    start_frame = self._get_other_frames(similarity_matrix,min_index_frame,-1)
                    end_frame = self._get_other_frames(similarity_matrix,max_index_frame,1)
                    # print('Start:End',start_frame,end_frame)
                    #Відкидаємо інтервали краї яких вишли за межі відео
                    if end_frame - start_frame < 2:
                        print('Out range')
                        continue

                    #Кінцева фільтрація  темних кадрів
                    count_black = 0 
                    X_b = np.stack(video[start_frame:end_frame])
                    Y_b = np.stack(img_vec_util.get_black())
                    similarity_matrix_b = cosine_similarity(X_b, Y_b)
                    for index in range(similarity_matrix_b.shape[0]):
                        for i,z in enumerate(similarity_matrix_b[index]):
                            if z > acuraci_black:
                                count_black +=1

                    #Якщо в інтервалі 50% темних кадрів, відкидаємо інтервал   
                    if count_black >= 0.50*(end_frame-start_frame): #end_frame-start_frame > 10 and 
                        print(r'50% black')
                        continue


                    #Кінцева фільтрація  білих кадрів
                    count_white = 0 
                    Y_b = np.stack(img_vec_util.get_white())
                    similarity_matrix_b = cosine_similarity(X_b, Y_b)
                    for index in range(similarity_matrix_b.shape[0]):
                        for i,z in enumerate(similarity_matrix_b[index]):
                            if z > acuraci_black:
                                count_white +=1

                    #Якщо в інтервалі 50% темних кадрів, відкидаємо інтервал   
                    if count_white >= 0.50*(end_frame-start_frame): #
                        print(r'50% white')
                        continue

                    advertising_intervals.append((int(start_frame*fps),int(end_frame*fps)))

        return self._reduce_list(advertising_intervals) 


    def find_overlay(self,overlay_info_list,fps=23):

        acuraci_ident =  0.9999999 #Точність для пошуку кадрів оверлеїв
        porog_find_count = 7 #кількість кадрів які повинен містити мінімальний оверлей
        overlay_intervals = collections.defaultdict(list) # Словник інтервалів знайдених оверлеїв, де ключом є тип реклами
        index_no_overlay = 3
        rekl = ['1xbet', 'betwinner', 'melbet', 'no_overlay', 'octopus', 'orka', 'spinwin']# Текстове представлення типів оверлеїв
        overlay_list = []
        overlay_type = []
        
        for i,overlay_info in enumerate(overlay_info_list):
            index_frame = np.argmax(overlay_info) # індекс типу оверлею
            predict_info = np.max(overlay_info) # оцінка принадлежності оверлею до заданого типу
            
            if index_frame  != index_no_overlay and predict_info > acuraci_ident: # пропуск всіх не оверлейних кадрів
                overlay_list.append(i)
                overlay_type.append(index_frame)#!!!!!!!!!!!!!!!
                #print(f'{i*fps}==>{index_frame}',rekl[index_frame], predict_info)
        
        start_i = 0#!!!!!!!!!!!!!!
        for reklama_list in self._breaks_list(overlay_list,fps,5):
            if len(reklama_list) < porog_find_count: #Відкидаємо списки в яких кількість кадрів не перевищує porog_find_count
                    #print(reklama_list)
                    print(f'{len(reklama_list)}<{porog_find_count}')  
                    start_i += len(reklama_list)#!!!!!!!!!!!!!!
                    continue
            
            #Пошук в інтервалі типу оверлею який найчастіше зустрічається
            end_i = start_i+len(reklama_list)
            overlays = overlay_type[start_i:end_i]# Виділення зрізу з типами оверлею для reklama_list
            count_type_overlay = collections.Counter(overlays)
            #Визначення типу оверлею який найчастіше зустрічається, цей тип і вважаю типом цілого reklama_list
            max_frame_overlay = -1
            curent_type_overlay = None
            for key,item in count_type_overlay.items():
                if item > max_frame_overlay:
                    max_frame_overlay = item
                    curent_type_overlay = key 

            #Відсікання з кінця кадрів тип яких не співпадає з типом цілого reklama_list
            index = -1
            while True:
                if overlays[index] != curent_type_overlay:
                    index -= 1
                else:
                    break
            if index != -1:
                reklama_list = reklama_list[:index+1]

            # print('i',start_i,end_i,curent_type_overlay)
            start_i = end_i

            #Розширюємо інтервали    
            min_index_frame = np.min(reklama_list)
            max_index_frame = np.max(reklama_list)
            #print(min_index_frame,max_index_frame)
            start_frame = self._get_other_frames_overlay(overlay_info_list,min_index_frame,-1,type_overlay=curent_type_overlay)
            end_frame = self._get_other_frames_overlay(overlay_info_list,max_index_frame,1,type_overlay=curent_type_overlay)
            # print('Start:End',start_frame,end_frame)

            overlay_intervals[rekl[curent_type_overlay]].append((int(start_frame *fps-fps//2),int(end_frame*fps+fps//2)))
        
        return overlay_intervals


    def _get_other_frames_overlay(self,overlay_info_list,start_index,step,type_overlay):
        """
        Уточнює позиці оверлею відносно  start_index
        overlay_info_list- список предікшенів від мережі по кадрах відео
        start_index - індекс кадру в якому точно реклама
        step - крок по кадрах:
                            1 - для зростання номерів
                            -1 - для спадання номерів
        Повертає початковий або кінцевий кадр реклами, в залежності від значення step
        """
        # index - поточний номер кадру відео
        index = start_index + step #Старт із знайденого фрейму зміщеного на step
        count_empty_step = 0 #Кількість кадрів підряд які не ідентифікувалися як реклама, для них count == 0
        acuraci_ident = 0.99
        empty_step = 3
        
        while True:
            count = 0
            if index < 1:
                return 0

            if index >= len(overlay_info_list):
                return len(overlay_info_list) - 1
            
            index_frame = np.argmax(overlay_info_list[index]) # індекс типу оверлею
            predict_info = np.max(overlay_info_list[index]) # оцінка принадлежності оверлею до заданого типу
            if index_frame  == type_overlay and predict_info > acuraci_ident: # пропуск всіх не оверлейних кадрів
                count += 1

            if count == 0:#
                count_empty_step += 1
                if count_empty_step > empty_step:#Поріг в 4 кадри які підряд можуть не ідентифікуватися
                    return index-step*count_empty_step
            else:
                count_empty_step = 0 

            index = index + step